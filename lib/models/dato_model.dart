class Datos {
  late List<Datos> products;

  Datos({required this.products});

  Datos.fromJson(Map<String, dynamic> json) {
    if (json['datos'] != null) {
      products = <Datos>[];
      json['datos'].forEach((v) {
        products.add(Datos.fromJson(v));
      });
    }
  }
}

class Dato {
  late String name;
  late String date;
  late String house;

  Dato({
    required this.name,
    required this.date,
    required this.house,
  });

  Dato.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    date = json['date'];
    house = json['house'];
  }
}
