import 'package:flutter/material.dart';
import 'package:seoltfer_p5/models/dato_model.dart';

class DetailsView extends StatelessWidget {
  final Dato dato;

  const DetailsView({super.key, required this.dato});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Detalles del Estudiante'),
        backgroundColor: Colors.deepPurple[900],
      ),
      body: LayoutBuilder(
        builder: (BuildContext context, BoxConstraints constraints) {
          if (constraints.maxWidth > 600) {
            return _buildWideContainers();
          } else {
            return _buildNormalContainer();
          }
        },
      ),
    );
  }

  Widget _buildWideContainers() {
    return Center(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          _buildDetailsContainer(),
          const SizedBox(width: 20),
          _buildImageContainer(),
        ],
      ),
    );
  }

  Widget _buildNormalContainer() {
    return SingleChildScrollView(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          _buildDetailsContainer(),
          const SizedBox(height: 20),
          _buildImageContainer(),
        ],
      ),
    );
  }

  Widget _buildDetailsContainer() {
    return Container(
      padding: const EdgeInsets.all(16.0),
      decoration: BoxDecoration(
        color: Colors.black.withOpacity(0.7),
        borderRadius: BorderRadius.circular(10),
        boxShadow: [
          BoxShadow(
            color: Colors.black.withOpacity(0.5),
            spreadRadius: 5,
            blurRadius: 7,
            offset: const Offset(0, 3),
          ),
        ],
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Nombre: ${dato.name}',
            style: const TextStyle(
              fontSize: 18,
              color: Colors.white,
            ),
          ),
          const SizedBox(height: 10),
          Text(
            'Casa: ${dato.house.toUpperCase()}',
            style: TextStyle(
              fontSize: 18,
              color: _getHouseColor(dato.house),
            ),
          ),
          const SizedBox(height: 10),
          Text(
            'Fecha: ${dato.date}',
            style: const TextStyle(
              fontSize: 18,
              color: Colors.white,
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildImageContainer() {
    return Container(
      width: 200,
      height: 200,
      decoration: BoxDecoration(
        color: Colors.transparent,
        borderRadius: BorderRadius.circular(10),
        image: const DecorationImage(
          image: AssetImage('assets/images/Hogwarts.jpg'),
          fit: BoxFit.cover,
        ),
      ),
    );
  }

  Color _getHouseColor(String house) {
    switch (house.toLowerCase()) {
      case 'grayfindor':
        return Colors.red;
      case 'haflepaf':
        return Colors.yellow;
      case 'reivenclau':
        return Colors.blue;
      case 'eslizerin':
        return Colors.green;
      default:
        return Colors.white;
    }
  }
}
