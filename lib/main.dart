import 'package:flutter/material.dart';
import 'package:seoltfer_p5/views/login_view.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) => const MaterialApp(
        title: 'P5 IPC',
        debugShowCheckedModeBanner: false,
        home: LoginView(),
      );
}
